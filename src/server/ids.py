import importlib
import pkgutil

from . import generators


__generators = {}


def id_generator(name, description):
    def id_generator_impl(function):
        __generators[name] = (name, description, function)
        return function
    return id_generator_impl


for _, mod_name, _ in pkgutil.iter_modules(generators.__path__):
    importlib.import_module(generators.__name__ + '.' + mod_name)


def get_generators():
    return __generators

