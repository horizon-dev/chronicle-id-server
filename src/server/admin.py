from django.contrib import admin

from .models import (
    HandleValue,
    HomedPrefix,
    UserAdminHandle,
    UserObjectHandle)


admin.site.register(HandleValue)
admin.site.register(HomedPrefix)
admin.site.register(UserAdminHandle)
admin.site.register(UserObjectHandle)
