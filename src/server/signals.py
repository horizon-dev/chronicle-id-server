import os
import base64

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models.signals import (post_save, pre_delete)
from django.dispatch import receiver

from . import models
from . import utils
from chronicle.handle import types
from chronicle.handle import __TEXT_ENCODING as TEXT_ENCODING


@receiver(
    post_save,
    sender=get_user_model(),
    dispatch_uid='signals.user_saved_handler')
def user_saved_handler(sender, instance, created, *args, **kwargs):
    if created:

        id = utils.get_user_handle(
            instance, prefix=settings.DEFAULT_HOME_PREFIX)

        email = types.Email(instance.email)

        seckey = types.SecKey(base64.b64encode(\
                os.urandom(32)).decode(TEXT_ENCODING))

        admin = types.Admin(types.Admin.AdminValue(
            handle_value=id,
            handle_index='',
            add_handle=False,
            delete_handle=False,
            add_na=False,
            delete_na=False,
            modify_value=True,
            delete_value=True,
            add_value=True,
            modify_admin=False,
            remove_admin=False,
            add_admin=False,
            authorized_read=True,
            list_handle=True,
            list_na=False))

        utils.create_user_admin_handle(id, instance, email, seckey, admin)

@receiver(
    pre_delete,
    sender=get_user_model(),
    dispatch_uid='signals.user_pre_delete_handler')
def user_pre_delete_handler(sender, instance, using, *args, **kwargs):
    for uah in list(models.UserAdminHandle.objects.filter(user=instance)):
        uah.email.delete()
        uah.seckey.delete()
        uah.admin.delete()

    for uoh in list(models.UserObjectHandle.objects.filter(user=instance)):
        uoh.url.delete()
        uoh.desc.delete()
        uoh.admin.delete()
