from django.conf import settings

import allauth.account.adapter

from . import models


class AllauthAccountAdaper(allauth.account.adapter.DefaultAccountAdapter):
    def populate_username(self, request, user):
        user.username = user.email


def get_user_handle(user, prefix=settings.DEFAULT_HOME_PREFIX):
    return prefix + '/' + user.email

def get_value_handle(val, prefix=settings.DEFAULT_HOME_PREFIX):
    return prefix + '/' + val

def create_user_admin_handle(handle, user, email, seckey, admin):

    h_email = models.HandleValue(
        handle=handle,
        index=email.preferred_index(),
        admin_read=True,
        admin_write=True,
        public_read=False,
        public_write=False)
    h_email.set_data(email)
    h_email.save()

    h_seckey = models.HandleValue(
        handle=handle,
        index=seckey.preferred_index(),
        admin_read=True,
        admin_write=True,
        public_read=False,
        public_write=False)
    h_seckey.set_data(seckey)
    h_seckey.save()

    admin.val.handle_index = h_seckey.index
    h_admin = models.HandleValue(
        handle=handle,
        index=admin.preferred_index(),
        admin_read=True,
        admin_write=True,
        public_read=True,
        public_write=False)
    h_admin.set_data(admin)
    h_admin.save()

    ah = models.UserAdminHandle(
        user=user,
        email=h_email,
        seckey=h_seckey,
        admin=h_admin)
    ah.save()
    return ah

def create_user_object_handle(handle, user, url, desc):

    h_url = models.HandleValue(
        handle=handle,
        index=url.preferred_index(),
        admin_read=True,
        admin_write=True,
        public_read=True,
        public_write=False)
    h_url.set_data(url)
    h_url.save()

    h_desc = models.HandleValue(
        handle=handle,
        index=desc.preferred_index(),
        admin_read=True,
        admin_write=True,
        public_read=True,
        public_write=False)
    h_desc.set_data(desc)
    h_desc.save()

    ah = models.UserAdminHandle.objects.filter(user=user)[0].admin.get_data()
    h_admin = models.HandleValue(
        handle=handle,
        index=ah.preferred_index(),
        admin_read=True,
        admin_write=True,
        public_read=True,
        public_write=False)
    h_admin.set_data(ah)
    h_admin.save()

    uoh = models.UserObjectHandle(
        user=user,
        url=h_url,
        desc=h_desc,
        admin=h_admin)
    uoh.save()
    return uoh
