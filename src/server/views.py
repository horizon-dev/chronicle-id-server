from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import (redirect, render)
from django.views.generic.base import (TemplateResponseMixin, View)

from allauth.utils import get_request_param
from allauth.account.adapter import get_adapter
from allauth.account.utils import get_next_redirect_url

from chronicle.handle import types

from . import models
from . import utils
from . import ids


class IndexView(TemplateResponseMixin, View):

    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        return self.render_to_response({
            'handles': models.UserObjectHandle.objects.filter(user=request.user),
            'generators' : ids.get_generators()
        })

    def post(self, request, *args, **kwargs):
        if request.POST['action'] == 'create':
            _, _, uuid_gen = ids.get_generators()[request.POST['generator']]
            id = utils.get_value_handle(uuid_gen())
            url = types.URL(request.POST['uri'])
            desc = types.Desc(request.POST['description'])
            utils.create_user_object_handle(id, self.request.user, url, desc)
        elif request.POST['action'] == 'delete':
            uoh = models.UserObjectHandle.objects.get(id=request.POST['id'])
            uoh.url.delete()
            uoh.desc.delete()
            uoh.admin.delete()
            uoh.delete()
        elif request.POST['action'] == 'update':
            uoh = models.UserObjectHandle.objects.get(id=request.POST['id'])
            url = types.URL(request.POST['uri'])
            desc = types.Desc(request.POST['description'])
            uoh.url.set_data(url)
            uoh.url.save()
            uoh.desc.set_data(desc)
            uoh.desc.save()
        return self.get(request, *args, **kwargs)

index = login_required(IndexView.as_view())


class DeleteAccountView(TemplateResponseMixin, View):

    template_name = "account/delete.html"
    redirect_field_name = "next"

    def get(self, *args, **kwargs):
        return self.render_to_response(self.get_context_data())

    def post(self, *args, **kwargs):
        url = self.get_redirect_url()
        self.delete_account()
        return redirect(url)

    def delete_account(self):
        user = self.request.user
        adapter = get_adapter(self.request)
        adapter.add_message(
            self.request,
            messages.SUCCESS,
            'account/messages/account_deleted.txt')
        adapter.logout(self.request)
        user.delete()

    def get_context_data(self, **kwargs):
        ctx = kwargs
        redirect_field_value = get_request_param(self.request,
                                                 self.redirect_field_name)
        ctx.update({
            "redirect_field_name": self.redirect_field_name,
            "redirect_field_value": redirect_field_value})
        return ctx

    def get_redirect_url(self):
        return (
            get_next_redirect_url(
                self.request,
                self.redirect_field_name) or get_adapter(
                    self.request).get_logout_redirect_url(
                        self.request))

account_delete = login_required(DeleteAccountView.as_view())


@login_required
def account_profile(request):
    return render(request, 'account/profile.html')
