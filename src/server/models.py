import json
from datetime import datetime
from datetime import timezone

from django.conf import settings
from django.db import models
from django.utils.timezone import now

from chronicle.handle import __TEXT_ENCODING as TEXT_ENCODING

from chronicle.handle.ioutil import (
    __INT_SIZE as INT_SIZE,
    __BYTE_ORDER as BYTE_ORDER,
    read_bytes,
    size_to_bytes
)

from chronicle.handle import types


class BinaryStringField(models.Field):

    description = 'UTF-8 encoded byte string'

    def db_type(self, connection):
        return 'BYTEA'

    def from_db_value(self, value, *args):
        if value is None:
            return value
        return value.tobytes().decode(TEXT_ENCODING)

    def get_db_prep_value(self, value, connection, prepared=False):
        return value.upper().encode(TEXT_ENCODING)


class DateTimeIntField(models.DateTimeField):

    description = 'DateTime stored as Unix Epoch integer'

    def db_type(self, connection):
        return 'INT4'

    def from_db_value(self, value, *args):
        if value is None:
            return value
        return datetime.utcfromtimestamp(value)

    def get_db_prep_value(self, value, connection, prepared=False):
        if value is None:
            return value
        return int(value.replace(tzinfo=timezone.utc).timestamp())


class HandleValuesField(models.Field):

    description = 'A list of handle values'

    def db_type(self, connection):
        return 'BYTEA'

    def from_db_value(self, value, *args):
        if not value:
            return None
        value = value.tobytes()
        count = int.from_bytes(value[0:4], BYTE_ORDER)
        offset = 4
        data = []
        while count > 0:
            count -= 1
            nameb = read_bytes(value, offset=offset)
            name = nameb.decode(TEXT_ENCODING)
            offset += len(nameb) + INT_SIZE
            idx = int.from_bytes(value[offset:offset + 4], BYTE_ORDER)
            offset += INT_SIZE
            data.append([name, idx])
        return json.dumps(data)

    def get_db_prep_value(self, value, connection, prepared=False):
        if not value:
            return bytes()
        values = json.loads(value)
        buffer = bytearray()
        buffer.extend(size_to_bytes(len(values)))
        for value in values:
            name = value[0].encode(encoding=TEXT_ENCODING)
            buffer.extend(size_to_bytes(len(name)))
            buffer.extend(name)
            idx = value[1]
            buffer.extend(size_to_bytes(idx))
        return bytes(buffer)


class HandleValue(models.Model):
    TTL_TYPE_RELATIVE = 0
    TTL_TYPE_ABSOLUTE = 1
    __ONE_DAY_IN_SECONDS = 60 * 60 * 24
    ttl_types = (
        (TTL_TYPE_RELATIVE, 'Relative'),
        (TTL_TYPE_ABSOLUTE, 'Absolute')
    )
    id = models.AutoField(primary_key=True)
    handle = BinaryStringField()
    index = models.IntegerField(db_column='idx')
    value_type = BinaryStringField(blank=True, null=True, db_column='type')
    data = models.BinaryField(blank=True, null=True)
    ttl_type = models.SmallIntegerField(
        null=True,
        verbose_name='TTL Type',
        choices=ttl_types,
        default=TTL_TYPE_RELATIVE)
    ttl = models.IntegerField(
        null=True,
        verbose_name='TTL',
        default=__ONE_DAY_IN_SECONDS)
    timestamp = DateTimeIntField(
        blank=True,
        null=True,
        default=now)
    references = HandleValuesField(blank=True, null=True, db_column='refs')
    admin_read = models.BooleanField(default=True)
    admin_write = models.BooleanField(default=True)
    public_read = models.BooleanField(db_column='pub_read', default=True)
    public_write = models.BooleanField(db_column='pub_write', default=False)

    def set_data(self, value):
        self.value_type = value.type_name()
        self.data = value.encode()

    def get_data(self):
        cls = types.lookup_class_for_handle_value_type(self.value_type)
        return cls.decode(self.data)

    class Meta:
        managed = False
        db_table = 'handles'
        unique_together = (('handle', 'index'),)


class HomedPrefix(models.Model):
    prefix = BinaryStringField(primary_key=True, db_column='na')

    class Meta:
        managed = False
        db_table = 'nas'
        verbose_name_plural = 'Homed prefixes'


class UserAdminHandle(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    email = models.ForeignKey(
        HandleValue,
        on_delete=models.CASCADE,
        related_name='user_admin_handle_email')
    seckey = models.ForeignKey(
        HandleValue,
        on_delete=models.CASCADE,
        related_name='user_admin_handle_seckey')
    admin = models.ForeignKey(
        HandleValue,
        on_delete=models.CASCADE,
        related_name='user_admin_handle_admin')


class UserObjectHandle(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    url = models.ForeignKey(
        HandleValue,
        on_delete=models.CASCADE,
        related_name='user_object_handle_url')
    desc = models.ForeignKey(
        HandleValue,
        on_delete=models.CASCADE,
        related_name='user_object_handle_desc')
    admin = models.ForeignKey(
        HandleValue,
        on_delete=models.CASCADE,
        related_name='user_object_handle_admin')
