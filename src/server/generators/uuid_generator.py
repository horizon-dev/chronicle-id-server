import uuid

from ..ids import id_generator


@id_generator(name='uuid', description='Generates a new UUID')
def new_uuid():
    return str(uuid.uuid4())
