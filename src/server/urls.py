from django.contrib.staticfiles.storage import staticfiles_storage
from django.http import Http404
from django.urls import re_path
from django.views.defaults import page_not_found
from django.views.generic.base import RedirectView

from . import views


urlpatterns = [
    re_path(
        '^favicon.ico$',
        RedirectView.as_view(
            url=staticfiles_storage.url('images/favicon.ico'),
            permanent=False),
        name="favicon"),
    re_path(
        '^account/profile/$',
        views.account_profile,
        name='account_profile'),
    re_path(
        '^account/delete/$',
        views.account_delete,
        name="account_delete"),
    re_path(
        '^$',
        views.index,
        name="index"),
    re_path(
        '.*',
        page_not_found,
        {'exception' : Http404()}),
]
