from django.contrib import admin
from django.http import Http404
from django.urls import (include, path, re_path)
from django.views.defaults import page_not_found


static_404 = {'exception' : Http404()}

urlpatterns = [
    path('admin/', admin.site.urls),
    path(
        'account/signup/',
        page_not_found,
        static_404),
    re_path(
        '^account/password/.*$',
        page_not_found,
        static_404),
    path(
        'account/inactive/',
        page_not_found,
        static_404),
    re_path(
        '^account/[^/]*email/.*$',
        page_not_found,
        static_404),
    path('account/', include('allauth.urls')),
    path('', include('server.urls')),
]
