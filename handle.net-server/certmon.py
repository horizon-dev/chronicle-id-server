#!/usr/bin/python3
import os.path
import http.client
import json
import shutil
import socket
import sys
import xmlrpc.client

from inotify.adapters import Inotify
from inotify.constants import (IN_CREATE, IN_MODIFY)

from cryptography.hazmat.backends import openssl
from cryptography.hazmat.primitives import serialization

from jwcrypto.jwk import JWK

from chronicle.handle import keyutil

def log(*args, **kwargs):
    print(*args, flush=True, **kwargs)

class UnixStreamHTTPConnection(http.client.HTTPConnection):
    def connect(self):
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.sock.connect(self.host)

class UnixStreamTransport(xmlrpc.client.Transport, object):
    def __init__(self, socket_path):
        self.socket_path = socket_path
        super(UnixStreamTransport, self).__init__()

    def make_connection(self, host):
        return UnixStreamHTTPConnection(self.socket_path)


server = xmlrpc.client.ServerProxy(
    'http://null', transport=UnixStreamTransport("/tmp/supervisord.sock"))

watch_dir = '/certs/handle.chronicle.horizon.ac.uk'
handle_dir = '/handle.net-server/servers/default'
handle_process_name = 'handle.net-server'

if not os.path.exists(watch_dir):
    log('Certificate directory not available, exiting.', file=sys.stderr)
    sys.exit(0)

def generate_keys():
    log('Generating keys')
    shutil.copyfile(
        os.path.join(watch_dir, 'fullchain.pem'),
        os.path.join(handle_dir, 'serverCertificate.pem'))
    shutil.copyfile(
        os.path.join(watch_dir, 'key.pem'),
        os.path.join(handle_dir, 'privkey.pem'))
    with open(os.path.join(handle_dir, 'privkey.pem'), 'rb') as f:
        privpem = f.read()
        privkey = serialization.load_pem_private_key(
            privpem, password=None, backend=openssl.backend)
        pubkey = privkey.public_key()
        pubpem = pubkey.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        with open(os.path.join(handle_dir, 'pubkey.pem'), 'wb') as o:
            o.write(pubpem)
    with open(os.path.join(handle_dir, 'privkey.bin'), 'wb') as f:
        f.write(keyutil.bytes_from_private_key(privkey))
    with open(os.path.join(handle_dir, 'pubkey.bin'), 'wb') as f:
        f.write(keyutil.bytes_from_public_key(pubkey))
    with open(os.path.join(handle_dir, 'siteinfo.json'), 'r') as f:
        siteinfo = json.load(f)
        jskey = JWK.from_pem(privpem)
        jskey_json = json.loads(jskey.export_public())
    siteinfo['servers'][0]['publicKey']['value'] = jskey_json
    with open(os.path.join(handle_dir, 'siteinfo.json'), 'w') as f:
        json.dump(siteinfo, f)

generate_keys()
try:
    server.supervisor.startProcess(handle_process_name, True)
except:
    log("Unexpected error:", sys.exc_info()[0])

i = Inotify()
i.add_watch(watch_dir, IN_CREATE | IN_MODIFY)
watch_filenames = ['fullchain.pem']
try:
    for event in i.event_gen():
        if event is not None:
            (_, _, _, filename) = event
            if filename in watch_filenames:
                log('Certificate modified')
                try:
                    server.supervisor.stopProcess(handle_process_name, True)
                except:
                    log("Unexpected error:", sys.exc_info()[0])
                try:
                    generate_keys()
                except:
                    log("Unexpected error:", sys.exc_info()[0])
                try:
                    server.supervisor.startProcess(handle_process_name, True)
                except:
                    log("Unexpected error:", sys.exc_info()[0])

finally:
    i.remove_watch(watch_dir)
