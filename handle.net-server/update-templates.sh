#!/bin/bash
base_path="net/handle/apps/servlet_proxy/resources/WEB-INF/html/"
mkdir -p $base_path
links="<p>\
<a href=\\\"https:\/\/chronicle.horizon.ac.uk\/terms.html\\\">Terms \&amp\; Conditions<\/a> | \
<a href=\\\"https:\/\/chronicle.horizon.ac.uk\/terms.html\\\">Privacy Policy<\/a> | \
<a href=\\\"https:\/\/chronicle.horizon.ac.uk\/terms.html\\\">Cookie Policy<\/a><\/p><\/body>"
for f in error novalues  query  response; do
    unzip -p handle.jar ${base_path}${f}.html | sed -e "s/<\/body>/${links}/" > ${base_path}${f}.html
done
zip -mr handle.jar net
