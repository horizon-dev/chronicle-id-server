#!/bin/sh
sleep 2
echo ${DEFAULT_HOME_PREFIX} > $HOME/.handle/local_nas
sed -i -e "s/{DEFAULT_HOME_PREFIX}/${DEFAULT_HOME_PREFIX}/" \
        -e "s/{POSTGRES_PASSWORD}/${POSTGRES_PASSWORD}/" \
        /handle.net-server/servers/default/config.dct
supervisord -c /handle.net-server/servers/default/supervisord.conf
