#!/bin/bash

sleep 2
pypy3 manage.py migrate
unset DJANGO_ADMIN_USER DJANGO_ADMIN_EMAIL DJANGO_ADMIN_PASSWORD
pypy3 manage.py collectstatic --noinput
gunicorn -b 0.0.0.0:80 -w 4 --timeout 600 $@ idserver.wsgi
