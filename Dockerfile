FROM pypy:3.6-7.1.0-stretch
ENV PYTHONUNBUFFERED 1
EXPOSE 80
RUN update-alternatives --install /usr/bin/python python /usr/local/bin/pypy3 3
RUN update-alternatives --install /usr/bin/python python /usr/bin/python2 2
RUN mkdir /chronicle
WORKDIR /chronicle
ADD requirements.txt /chronicle/
RUN pip install -r requirements.txt \
    && rm requirements.txt
ADD src /chronicle/
ADD docker-entrypoint.sh /chronicle
ENTRYPOINT [ "/chronicle/docker-entrypoint.sh" ]
