CREATE TABLE nas (
  na BYTEA NOT NULL,
  PRIMARY KEY(na)
);

CREATE TABLE handles (
  id SERIAL PRIMARY KEY,
  handle BYTEA NOT NULL,
  idx int4 NOT NULL,
  type BYTEA,
  data BYTEA,
  ttl_type INT2,
  ttl INT4,
  timestamp INT4,
  refs BYTEA,
  admin_read BOOL,
  admin_write BOOL,
  pub_read BOOL,
  pub_write BOOL,
  UNIQUE (handle, idx)
);

CREATE INDEX dataindex on handles ( data );

CREATE INDEX handleindex on handles ( handle );
