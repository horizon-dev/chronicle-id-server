# Chronicle ID Platform Server

> Lifetime tracking for physical artefacts.

This is the work in progress Chronicle ID server. It is intended for creating
and administering identifiers in the Chronicle system.

## Requirements

* Required
    * [Docker](https://www.docker.com/)
    * [Docker Compose](https://docs.docker.com/compose/)

## Configuration

### Environment Variables

The following environment variables are available to configure various aspects
of the server configuration:

* **DJANGO_ADMIN_USER** (optional) - See **DJANGO_ADMIN_PASSWORD**.
* **DJANGO_ADMIN_EMAIL** (optional) - See **DJANGO_ADMIN_PASSWORD**
* **DJANGO_ADMIN_PASSWORD** (optional) - If the three **DJANGO_ADMIN_**
    variables are set, a Django admin user will be created when the application
    is **first run** using the variables.
* **DJANGO_DEBUG** (optional) - See [DEBUG in the Django documentation](
    https://docs.djangoproject.com/en/1.11/ref/settings/#debug).
* **DJANGO_LANGUAGE_CODE** (optional) - See [LANGUAGE_CODE in the Django documentation](
    https://docs.djangoproject.com/en/1.11/ref/settings/#language-code).
* **DJANGO_SECRET_KEY** (required) - See [SECRET_KEY in the Django documentation](
    https://docs.djangoproject.com/en/1.11/ref/settings/#secret-key).
* **DJANGO_SITE_NAME** (optional) - Set this to the domain name of the server.
* **POSTGRES_PASSWORD** (optional) - Password to set for the database user.
* **DEFAULT_HOME_PREFIX** (required) - Default Handle server homed prefix.

### AllAuth Login Providers

This application uses [Django-AllAuth](
https://www.intenct.nl/projects/django-allauth/) for OpenID and social login.
By default, no providers are enabled. To enable a provider, the fully qualified
name of the provider class must be put in ```etc/allauth_enabled_providers```
(create the file if it does not exist), each provider should be on a separate
line. e.g.,

```
allauth.socialaccount.providers.facebook
allauth.socialaccount.providers.github
```

Refer to the [AllAuth documentation(
https://django-allauth.readthedocs.io/en/latest/index.html) for a complete
list of providers.

## Running

To start, simply

```sh
docker-compose up -d
```

See [https://docs.docker.com/compose/](https://docs.docker.com/compose/) for
more information on ```docker-compose```.

## Release History

* 0.1.0 - 2018/02/15
    * Initial version
    * Basic account management
    * Basic handle CRUD methods

* 0.0.1
    * Work in progress

## Meta

© 2017 Horizon Digital Economy Research
[www.horizon.ac.uk](https://www.horizon.ac.uk)

[https://bitbucket.org/horizon-dev/chronicle-id-server](
https://bitbucket.org/horizon-dev/chronicle-id-server)

## Contributing

1. Fork it (<https://bitbucket.org/horizon-dev/chronicle-id-server>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
